﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Radegast;
using OpenMetaverse;

namespace Radegast.Plugin.SecurityBot
{
    [Radegast.Plugin(Name="SecurityBot Plugin", Description="Region bans people trying to crash the region.", Version="1.0")]
    public class SecurityBotPlugin : IRadegastPlugin
    {
        private RadegastInstance Instance;
        private GridClient Client { get { return Instance.Client; } }

        private string version = "1.0";
        private UUID master = UUID.Zero;

        public void StartPlugin(RadegastInstance inst)
        {
            Instance = inst;
            Instance.MainForm.TabConsole.DisplayNotificationInChat("SecurityBot loaded");

            // Ok, we want to answer to IMs as well
            Client.Self.IM += new EventHandler<InstantMessageEventArgs>(Self_IM);
        }

        public void StopPlugin(RadegastInstance instance)
        {
            Client.Self.IM -= new EventHandler<InstantMessageEventArgs>(Self_IM);
        }

        void Self_IM(object sender, InstantMessageEventArgs e)
        {
            if (Instance.MainForm.InvokeRequired)
            {
                Instance.MainForm.BeginInvoke(new MethodInvoker(() => Self_IM(sender, e)));
                return;
            }

            if (e.IM.Dialog == InstantMessageDialog.MessageFromObject)
            {
                var parts = e.IM.Message.Split(new String[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length > 1)
                {
                    if (parts[0].ToLower() == "regionban")
                    {
                        Client.Self.InstantMessage(new UUID(parts[1]), "I banned you because your script count is too high. Contact the region manager to be unbanned.");

                        Client.Self.InstantMessage(new UUID("a9904b1e-3a66-4e3c-845e-d5229b2a8319"), "I ejected " + parts[2] + " for high scripts at '" + Client.Network.CurrentSim.Name + "'.");
                        if(master != UUID.Zero)
                            Client.Self.InstantMessage(master, "I ejected " + parts[2] + " for high scripts at '" + Client.Network.CurrentSim.Name + "'.");

                        Client.Estate.BanUser(new UUID(parts[1]), true);
                        Client.Estate.KickUser(new UUID(parts[1]));
                    }
                }
            }

            if (e.IM.Dialog == InstantMessageDialog.MessageFromAgent
                && !Instance.Groups.ContainsKey(e.IM.IMSessionID)  // Message is not group IM (sessionID == groupID)
                && e.IM.BinaryBucket.Length < 2                    // Session is not ad-hoc friends conference
                && e.IM.FromAgentName != "Second Life"             // Not a system message
                )
            {
                if (e.IM.Message == "set owner turnip")
                {
                    master = e.IM.FromAgentID;
                    Instance.Netcom.SendInstantMessage(
                        string.Format("Hello {0}, i'll notify you when I region ban an avatar for attempting a crash", e.IM.FromAgentName),
                        e.IM.FromAgentID,
                        e.IM.IMSessionID);
                }
            }
        }
    }
}
